function filterBy(arr, dataType) {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    if (typeof arr[i] !== dataType) {
      result.push(arr[i]);
    }
  }
  return result;
}
const inputArray = ["hello", "world", 23, "23", null];
const dataTypeToFilter = "string";
const filteredArray = filterBy(inputArray, dataTypeToFilter);
console.log(filteredArray);
